import React from 'react'
import { render } from 'react-dom'
import { HashRouter, Route, Link } from "react-router-dom";
import { Provider, connect } from 'react-redux';

const ReactMenu = () => {
  return (
    <ul>
      <li><Link to="/">Home</Link></li>
      <li><Link to="/hello">Hello</Link></li>
    </ul>
  );
};
const ReactHome = () => <h2>React Home</h2>;
const ReactHello = () => <h2>React Hello!</h2>;
const ReactCounter = ({ count, increment }) => (
  <div>
    <button onClick={increment}>increment</button>
    <span>count: {count}</span>
  </div>
);
const ConnectedCounter = connect(
    (state) => ({ count: state.counter }),
    (dispatch) => ({ increment: () => dispatch({ type: 'INCREMENT' }) })
)(ReactCounter);

const ReactRoot = () => {
  return (
    <div className="app-content">
      <h1>React App</h1>
      <ReactMenu />
      <Route path="/" exact component={ReactHome} />
      <Route path="/hello" component={ReactHello} />
      <ConnectedCounter />
    </div>
  );
};

const ReactApp = ({store}) => (
  <Provider store={store}>
    <HashRouter hashType="hashbang">
      <ReactRoot />
    </HashRouter>
  </Provider>
);

export default (domContainer, store) => {
  render(<ReactApp store={store} />, domContainer);
};
