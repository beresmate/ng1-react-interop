import angular from "angular";
import '@uirouter/angularjs';
import 'ng-redux';

const angularMenuCmp = {
  name: 'angularMenu',
  template: `
    <ul>
      <li><a ui-sref="angularhome">Home</a></li>
      <li><a ui-sref="angularhello">Hello</a></li>
    </ul>
   `
};

const angularCounterCmp = {
  name: 'angularCounter',
  template: `
    <button ng-click="$ctrl.increment()">increment</button>
    <span>count: {{ $ctrl.count }}</span>
  `,
  controllerAs: '$ctrl',
  controller: function($ngRedux) {
    this.increment = () => $ngRedux.dispatch({ type: 'INCREMENT' });
    this.count = $ngRedux.getState().counter;

    $ngRedux.subscribe(() => {
      this.count = $ngRedux.getState().counter;
    });
  }
};

const angularRootState = {
  name: 'angularRoot',
  abstract: true,
  template: `
    <div class="app-content">
      <h1>Angular App</h1>
      <angular-menu></angular-menu>
      <ui-view></ui-view>
      <angular-counter></angular-counter>
    </div>
  `
};

const angularHomeState = {
  name: 'angularhome',
  parent: 'angularRoot',
  url: '/',
  template: `<h2>Angular Home</h2>`
};

const angularHelloState = {
  name: 'angularhello',
  parent: 'angularRoot',
  url: '/hello',
  template: `<h2>Angular Hello!</h2>`
};

function uiRouterConfig($stateProvider) {
  $stateProvider.state(angularRootState);
  $stateProvider.state(angularHomeState);
  $stateProvider.state(angularHelloState);
}

export default (domContainer, store) => {
  angular
    .module('angulardemo', ['ui.router', 'ngRedux'])
    .config(uiRouterConfig)
    .component('angularMenu', angularMenuCmp)
    .component('angularCounter', angularCounterCmp)
    .config(($ngReduxProvider) => {
      $ngReduxProvider.provideStore(store);
    });

  angular.bootstrap(domContainer, ['angulardemo']);
};
