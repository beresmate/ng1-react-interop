import { applyMiddleware, combineReducers, createStore } from "redux";
import { createLogger } from "redux-logger";

const logger = createLogger({
  collapsed: true
});

const counter = (state = 0, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1;
    default:
      return state;
  }
};

const reducer = combineReducers({
  counter
});

export default createStore(reducer, applyMiddleware(logger));
