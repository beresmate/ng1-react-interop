import store from './shared/store';
import startAngular from './angular/angularApp';
import startReact from './react/reactApp';
import './app.css';

startAngular(document.getElementById('angular-container'), store);
startReact(document.getElementById('react-container'), store);
